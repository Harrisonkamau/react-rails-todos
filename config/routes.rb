Rails.application.routes.draw do
  root 'todos#index'

  get '*page', to: 'todos#index', constraints: -> (req) do
    !req.xhr? && req.format.html?
  end
end
