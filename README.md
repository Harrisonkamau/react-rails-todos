# Todos-React-App

## Steps
1. Clone the repo
2. Install dependencies:
  - `bundle install`
  - `yarn install` (but if you are using then run: `npm install`)

3. Setup the Backend DB
  - `rails db:setup` (to run both `rails db:create` & `rails db:migrate`)
4. Create Sample todos
  - `rake todos:create`

5. Run dev servers
- Webpack: `./bin/webpack-dev-server`
- Rails server: `bin/rails s`

6. Visit `localhost:3000` to see all the created todos
