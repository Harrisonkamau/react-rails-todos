import React from 'react';
import { NavLink } from 'react-router-dom';

export default function Navbar() {
  const activeStyle = {
    color: 'orange',
  };

  return (
    <nav>
      <ul className='nav'>
        <li className='nav-item'>
          <NavLink to='/' exact className='nav-link' activeStyle={activeStyle}>Home</NavLink>
        </li>
        <li className='nav-item'>
          <NavLink to='/new-todo' exact className='nav-link' activeStyle={activeStyle}>New Todo</NavLink>
        </li>
      </ul>
    </nav>
  )
}
