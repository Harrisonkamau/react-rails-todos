import React from 'react';
import { Link } from 'react-router-dom';

export default function NotFound() {
  return (
    <div>
      <h5>Not Found - 404 </h5>
      <p>
        The page you are looking for does not exist.
        <Link to='/'>take me home please</Link>
      </p>
    </div>
  );
}
