import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { TodoList, CreateTodo } from './Todos';
import { NotFound, NavBar } from './common';

import 'jquery/dist/jquery';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'bootstrap/dist/css/bootstrap.min';


function App(props) {
  console.log('Hello world');
  return (
    <Router>
      <NavBar />
      <Switch>
        <Route exact path='/'>
          <TodoList todos={props.todos}/>
        </Route>
        <Route exact path='/new-todo' component={CreateTodo}/>
        <Route component={NotFound}/>
      </Switch>
    </Router>
  );
}

export default App;
