import React from 'react';
import './TodosSearch.css';

export default function TodosSearch({ searchQuery, setSearchQuery }) {
  return (
    <div className='container todosSearch'>
      <div className='row'>
      <div className='col-sm-4'></div>
        <div className='col-sm-4'>
          <input
            placeholder='Search by title'
            className='form-control'
            type='text'
            value={searchQuery}
            onChange={(event) => setSearchQuery(event.target.value)}
          />
        </div>
      </div>
    </div>
  );
}
