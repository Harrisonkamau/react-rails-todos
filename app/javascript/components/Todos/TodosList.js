import React, { useState } from 'react';
import TodosSearch from './TodoSearch';

function TodosList({ todos }) {
  const [ searchQuery, setSearchQuery ] = useState('');

  return (
    <div>
      <TodosSearch
        searchQuery={searchQuery}
        setSearchQuery={setSearchQuery}
      />
      <div className='container'>
        <div className='row'>
          {todos
            .filter((t) => {
              const targetString = t.title.toLowerCase();
              return searchQuery.length === 0
              ? true
              : targetString.includes(searchQuery.toLowerCase());
            })
            .map((todo) => {
                return (
                  <div className='col-sm-4' key={todo.id}>
                    <div className='card'>
                        <div className='card-body'>
                            <h4 className='card-title'>{todo.title}</h4>
                            <p className='card-text'>{todo.description}
                          </p>
                        </div>
                    </div>
                </div>
                )
              })
          }
        </div>
      </div>
    </div>
  );
}

export default TodosList;
