# frozen_string_literal: true

namespace 'todos' do
  task create: :environment do
    sample_todos.each do |todo|
      Todo.create!(todo)
    end
  end
end

private

def sample_todos
  [
    {
      title: 'Write React',
      description: 'I am going to write React all of my life'
    },
    {
      title: 'Rails Master',
      description: 'I have always known myself as a Rails master'
    },
    {
      title: 'Watch Soccer',
      description: 'EPL is in my blood'
    }
  ]
end
